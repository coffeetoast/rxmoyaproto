# README #

functional reactive programming (FRP)를 위한 ProtoType 입니다. 
기본적인 Binding은 RxSwift, RxCocoa를 이용하였고, Networking Layer는 Moya, Parsing에는 Moya-ModelMapper 를 이용하였습니다. 

Asynchronous Data Flow 관점에서 MVVM,POP 모델로 프로그래밍하는데 도움이 되었으면 좋겠습니다. 