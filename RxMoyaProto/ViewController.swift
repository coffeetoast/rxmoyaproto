//
//  ViewController.swift
//  RxMoyaProto
//
//  Created by SungJae Lee on 2017. 9. 12..
//  Copyright © 2017년 SungJae Lee. All rights reserved.
//

import Moya
import Moya_ModelMapper
import UIKit
import RxCocoa
import RxSwift

class ViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    let disposeBag = DisposeBag()
    var provider: RxMoyaProvider<GitHub>!
    var latestRepositoryName: Observable<String> {
        return searchBar
            .rx.text
            .filter { $0!.characters.count > 0 }
            .throttle(0.5, scheduler: MainScheduler.instance)
            .filterNil()
            .distinctUntilChanged()
    }
    
    var viewModel: ViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupRx()
    }
    
    func setupRx() {
        provider = RxMoyaProvider<GitHub>()
        
        viewModel = ViewModel(provider: provider, repositoryName: latestRepositoryName)
        
        viewModel
            .fetchRepository()
            .drive(tableView.rx.items) { tableView, row, item in
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: IndexPath(row: row, section: 0))
                cell.textLabel?.text = item.name
                cell.detailTextLabel?.text = item.url
                
                return cell
            }
            .addDisposableTo(disposeBag)
        
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                if self?.searchBar.isFirstResponder == true {
                    self?.view.endEditing(true)
                }
            }).addDisposableTo(disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

