//
//  Model.swift
//  RxMoyaProto
//
//  Created by SungJae Lee on 2017. 9. 12..
//  Copyright © 2017년 SungJae Lee. All rights reserved.
//

import Foundation
import Mapper

struct Repository: Mappable {
    
    let name: String
    let url: String
    
    init(map: Mapper) throws {
        try name = map.from("name")
        try url = map.from("url")
    }
}

//struct Issue: Mappable {
//    
//    let identifier: Int
//    let number: Int
//    let title: String
//    let body: String
//    
//    init(map: Mapper) throws {
//        try identifier = map.from("id")
//        try number = map.from("number")
//        try title = map.from("title")
//        try body = map.from("body")
//    }
//}
