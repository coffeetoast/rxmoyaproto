//
//  ViewModel.swift
//  RxMoyaProto
//
//  Created by SungJae Lee on 2017. 9. 12..
//  Copyright © 2017년 SungJae Lee. All rights reserved.
//

import Foundation
import Moya
import Mapper
import Moya_ModelMapper
import RxOptional
import RxSwift
import RxCocoa

struct ViewModel {
    
    let provider: RxMoyaProvider<GitHub>
    let repositoryName: Observable<String>
    
    func fetchRepository() -> Driver<[Repository]> {
        return repositoryName
            .subscribeOn(MainScheduler.instance)
            .do(onNext: { (name) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            })
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMapLatest { name -> Observable<[Repository]?> in
                print("Name: \(name)")
                return self.findRepository(name: name)
            }
            .subscribeOn(MainScheduler.instance)
            .do(onNext: { (name) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
            .replaceNilWith([])
            .asDriver(onErrorJustReturn: [])
    }
    
    
    internal func findRepository(name: String) -> Observable<[Repository]?> {
        return self.provider
            .request(GitHub.repos(username: name))
            .debug()
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .mapArrayOptional(type: Repository.self)
    }
}
